$(".slider").owlCarousel({
  loop: false,
  autoplay: false,
  autoplayTimeout: 3000,
  autoplayHoverPause: true,
  responsive: {
    0:{
      items:1,
      nav: false
    },
    600:{
      items:2,
      nav: false
    },
    1000:{
      items:3,
      nav: false
    }
  }
});

$(".carousel2").owlCarousel({
    margin: 10,
    loop: true,
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsive: {
      0:{
        items:1,
        nav: false
      },
      600:{
        items:2,
        nav: false
      },
      1000:{
        items:3,
        nav: false
      }
    }
  });

  // menu mobile
  var btnAbrir = document.querySelector('.btn-menu');
  var btnClose = document.querySelector('.btn-close')
  var closePag = document.querySelector('.close-pag')
  var btnInsc = document.querySelector('.btn1')
  
  btnAbrir.addEventListener('click',()=>{
              let menu = document.querySelector('.menu-mobile');
              
                  menu.style.left = "0px";
                  btnAbrir.style.display = "none";
                  btnClose.style.display = "block";
              
          });

 btnClose.addEventListener('click',(e)=>{
              e.preventDefault();
              let menu = document.querySelector('.menu-mobile');
              
                  menu.style.left = "-100%";
                  btnAbrir.style.display = "block";
                  btnClose.style.display = "none";
          });
          
btnInsc.addEventListener('click',()=>{
            let menu = document.querySelector('.jannela-pag');
            menu.style.display = "block";
            
        });

  closePag.addEventListener('click',(e)=>{
              e.preventDefault();
              let menu = document.querySelector('.jannela-pag');
              menu.style.display = "none";
          })